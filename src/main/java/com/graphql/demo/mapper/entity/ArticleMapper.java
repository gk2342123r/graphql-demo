package com.graphql.demo.mapper.entity;

import com.graphql.demo.entity.ArticleEntity;
import com.graphql.demo.generated.types.ArticleInput;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants.ComponentModel;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = ComponentModel.SPRING)
public interface ArticleMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    void fill(ArticleInput input, @MappingTarget ArticleEntity article);
}
