package com.graphql.demo.mapper.graphql;

import com.graphql.demo.entity.ArticleGroupEntity;
import com.graphql.demo.generated.types.ArticleGroupType;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface ArticleGroupTypeMapper {
    ArticleGroupType map(ArticleGroupEntity article);
}
