package com.graphql.demo.mapper.graphql;

import com.graphql.demo.entity.ArticleEntity;
import com.graphql.demo.generated.types.ArticleType;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants.ComponentModel;

import java.util.List;

@Mapper(componentModel = ComponentModel.SPRING, uses = {ArticleGroupTypeMapper.class})
public interface ArticleTypeMapper {

  ArticleType map(ArticleEntity article);

  List<ArticleType> map(List<ArticleEntity> articles);
}
