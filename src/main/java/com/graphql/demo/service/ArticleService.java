package com.graphql.demo.service;

import com.graphql.demo.entity.ArticleEntity;
import com.graphql.demo.entity.ArticleGroupEntity;
import com.graphql.demo.generated.types.ArticleInput;
import com.graphql.demo.mapper.entity.ArticleMapper;
import com.graphql.demo.repository.ArticleGroupRepository;
import com.graphql.demo.repository.ArticleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ArticleService {
    private final ArticleRepository articleRepository;
    private final ArticleGroupRepository articleGroupRepository;
    private final ArticleMapper articleMapper;

    public ArticleEntity create(ArticleInput input) {
        ArticleEntity article = new ArticleEntity();

        articleMapper.fill(input, article);

        if (!input.getArticleGroupId().isEmpty()) {
            ArticleGroupEntity group = articleGroupRepository.findById(input.getArticleGroupId()).orElseThrow();

            article.setArticleGroup(group);
        }

        articleRepository.saveAndFlush(article);

        return article;
    }

    public ArticleEntity update(String id, ArticleInput input) {
        ArticleEntity article = findOrFail(id);

        articleMapper.fill(input, article);

        if (!input.getArticleGroupId().isEmpty()) {
            ArticleGroupEntity group = articleGroupRepository.findById(input.getArticleGroupId()).orElseThrow();

            article.setArticleGroup(group);
        }

        articleRepository.saveAndFlush(article);

        return article;
    }

    public void delete(String id) {
        ArticleEntity article = findOrFail(id);

        articleRepository.delete(article);
    }

    public ArticleEntity findOrFail(String id) {
        return articleRepository.findById(id).orElseThrow();
    }
}
