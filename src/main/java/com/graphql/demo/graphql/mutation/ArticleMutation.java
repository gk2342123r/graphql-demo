package com.graphql.demo.graphql.mutation;

import com.graphql.demo.entity.ArticleEntity;
import com.graphql.demo.generated.DgsConstants.MUTATION;
import com.graphql.demo.generated.types.ArticleInput;
import com.graphql.demo.generated.types.ArticleType;
import com.graphql.demo.mapper.graphql.ArticleTypeMapper;
import com.graphql.demo.service.ArticleService;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsMutation;
import com.netflix.graphql.dgs.InputArgument;
import lombok.RequiredArgsConstructor;

@DgsComponent
@RequiredArgsConstructor
public class ArticleMutation {
    private final ArticleService articleService;

    private final ArticleTypeMapper articleTypeMapper;

    @DgsMutation(field = MUTATION.ArticleCreate)
    public ArticleType create(@InputArgument ArticleInput input) {

        ArticleEntity article = articleService.create(input);

        return articleTypeMapper.map(article);
    }

    @DgsMutation(field = MUTATION.ArticleUpdate)
    public ArticleType update(@InputArgument String id, @InputArgument ArticleInput input) {

        ArticleEntity article = articleService.update(id, input);

        return articleTypeMapper.map(article);
    }

    @DgsMutation(field = MUTATION.ArticleDelete)
    public boolean delete(@InputArgument String id) {

        articleService.delete(id);

        return true;
    }
}
