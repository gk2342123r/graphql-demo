package com.graphql.demo.graphql.query;

import com.graphql.demo.entity.ArticleEntity;
import com.graphql.demo.generated.types.ArticleType;
import com.graphql.demo.mapper.graphql.ArticleTypeMapper;

import com.graphql.demo.service.ArticleService;
import lombok.RequiredArgsConstructor;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsQuery;
import com.netflix.graphql.dgs.InputArgument;

@DgsComponent
@RequiredArgsConstructor
public class ArticleQuery {
    private final ArticleService articleService;
    private final ArticleTypeMapper articleTypeMapper;

    @DgsQuery
    public ArticleType article(@InputArgument String id) {
        ArticleEntity article = articleService.findOrFail(id);

        return articleTypeMapper.map(article);
    }
}
