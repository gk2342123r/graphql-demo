package com.graphql.demo.repository;

import com.graphql.demo.entity.ArticleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleRepository
        extends JpaRepository<ArticleEntity, String>,
        JpaSpecificationExecutor<ArticleEntity> {

    ArticleEntity findBySlug(String slug);
}
