package com.graphql.demo.repository;

import com.graphql.demo.entity.ArticleGroupEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleGroupRepository
        extends JpaRepository<ArticleGroupEntity, String>,
        JpaSpecificationExecutor<ArticleGroupEntity> {
}
