ALTER TABLE articles
    ADD COLUMN article_group_id VARCHAR(36);

CREATE INDEX articles_article_group_id_index ON articles (article_group_id);

ALTER TABLE articles
    ADD CONSTRAINT articles_article_group_id_foreign FOREIGN KEY (article_group_id) REFERENCES article_groups (id) ON UPDATE CASCADE ON DELETE SET NULL;