CREATE TABLE articles
(
    id         VARCHAR(36) NOT NULL PRIMARY KEY,
    title      VARCHAR(255) NOT NULL,
    slug       VARCHAR(255) NOT NULL,
    text       VARCHAR,
    published  BOOLEAN NOT NULL,
    created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    updated_at TIMESTAMP WITHOUT TIME ZONE NOT NULL
);

CREATE UNIQUE INDEX articles_slug_unique ON articles (slug);
