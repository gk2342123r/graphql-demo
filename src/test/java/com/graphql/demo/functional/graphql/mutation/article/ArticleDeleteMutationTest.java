package com.graphql.demo.functional.graphql.mutation.article;

import com.graphql.demo.entity.ArticleEntity;
import com.graphql.demo.functional.graphql.base.BaseArticleTest;
import com.graphql.demo.generated.DgsConstants.MUTATION;
import com.graphql.demo.generated.client.ArticleDeleteGraphQLQuery;
import com.netflix.graphql.dgs.client.codegen.GraphQLQueryRequest;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static java.lang.Boolean.TRUE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class ArticleDeleteMutationTest extends BaseArticleTest {
    @Test
    public void articleDeleteSuccess() {

        ArticleEntity article = articleFactory.create();

        GraphQLQueryRequest queryRequest = new GraphQLQueryRequest(new ArticleDeleteGraphQLQuery(article.getId(), Set.of()));

        Boolean actualResult = dgsQueryExecutor.executeAndExtractJsonPathAsObject(
                queryRequest.serialize(),
                "data." + MUTATION.ArticleDelete, Boolean.class
        );

        assertEquals(TRUE, actualResult);

        assertFalse(articleRepository.findById(article.getId()).isPresent());
    }
}
