package com.graphql.demo.functional.graphql.base;

import com.graphql.demo.factory.article.ArticleFactory;
import com.graphql.demo.factory.article.ArticleGroupFactory;
import com.graphql.demo.repository.ArticleRepository;
import com.netflix.graphql.dgs.DgsQueryExecutor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.annotation.DirtiesContext;

@SpringBootTest
@DirtiesContext
public abstract class BaseArticleTest{
    @Autowired
    protected ApplicationContext applicationContext;
    @Autowired
    protected DgsQueryExecutor dgsQueryExecutor;
    @Autowired
    protected ArticleRepository articleRepository;
    @Autowired
    protected ArticleFactory articleFactory;
    @Autowired
    protected ArticleGroupFactory articleGroupFactory;

    @AfterEach
    @BeforeEach
    void clean() {
        articleRepository.deleteAllInBatch();
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }
}
