package com.graphql.demo.functional.graphql.mutation.article;

import com.graphql.demo.entity.ArticleEntity;
import com.graphql.demo.entity.ArticleGroupEntity;
import com.graphql.demo.functional.graphql.base.BaseArticleTest;
import com.graphql.demo.generated.DgsConstants.MUTATION;
import com.graphql.demo.generated.client.ArticleUpdateGraphQLQuery;
import com.graphql.demo.generated.client.ArticleUpdateProjectionRoot;
import com.graphql.demo.generated.types.ArticleInput;
import com.graphql.demo.generated.types.ArticleType;
import com.netflix.graphql.dgs.client.codegen.GraphQLQueryRequest;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;

public class ArticleUpdateMutationTest extends BaseArticleTest {
    @Test
    public void articleUpdateSuccess() {

        ArticleEntity article = articleFactory.create();
        ArticleGroupEntity group = articleGroupFactory.create();

        String newTitle = "new title";
        String newDescription = "new text";
        String newSlug = "new-title";

        assertSame(null, articleRepository.findBySlug(newSlug));

        ArticleInput input =
                ArticleInput.newBuilder().title(newTitle).text(newDescription).slug(newSlug).articleGroupId(group.getId()).build();

        ArticleType articleType =
                dgsQueryExecutor.executeAndExtractJsonPathAsObject(
                        new GraphQLQueryRequest(
                                ArticleUpdateGraphQLQuery.newRequest()
                                        .id(article.getId())
                                        .input(input)
                                        .build(),
                                new ArticleUpdateProjectionRoot()
                                        .id()
                                        .title()
                                        .text()
                                        .slug()
                                        .published()
                                        .createdAt()
                                        .updatedAt()
                                        .articleGroup().id().name().published().createdAt().updatedAt())
                                .serialize(),
                        "data." + MUTATION.ArticleUpdate,
                        ArticleType.class);

        assertNotNull(articleType);

        assertThat(articleType)
                .hasFieldOrPropertyWithValue("title", newTitle)
                .hasFieldOrPropertyWithValue("text", newDescription)
                .hasFieldOrPropertyWithValue("slug", newSlug);

        assertThat(articleType.getArticleGroup())
                .hasFieldOrPropertyWithValue("id", group.getId())
                .hasFieldOrPropertyWithValue("name", group.getName());

        ArticleEntity updatedArticle = articleRepository.findBySlug(newSlug);

        assertThat(updatedArticle)
                .hasFieldOrPropertyWithValue("title", newTitle)
                .hasFieldOrPropertyWithValue("slug", newSlug);
    }
}
