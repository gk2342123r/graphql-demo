package com.graphql.demo.functional.graphql.query.article;

import com.graphql.demo.entity.ArticleEntity;
import com.graphql.demo.functional.graphql.base.BaseArticleTest;
import com.graphql.demo.generated.DgsConstants.QUERY;
import com.graphql.demo.generated.client.ArticleGraphQLQuery;
import com.graphql.demo.generated.client.ArticleProjectionRoot;
import com.graphql.demo.generated.types.ArticleType;
import com.netflix.graphql.dgs.client.codegen.GraphQLQueryRequest;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ArticleQueryTest extends BaseArticleTest {
    @Test
    public void articleShowSuccess() {
        ArticleEntity article = articleFactory.create();

        ArticleType articleType =
                dgsQueryExecutor.executeAndExtractJsonPathAsObject(
                        new GraphQLQueryRequest(
                                ArticleGraphQLQuery.newRequest().id(article.getId()).build(),
                                new ArticleProjectionRoot().id().title().slug().text().published())
                                .serialize(),
                        "data." + QUERY.Article,
                        ArticleType.class);

        assertThat(articleType)
                .isNotNull()
                .hasFieldOrPropertyWithValue("title", article.getTitle())
                .hasFieldOrPropertyWithValue("text", article.getText())
                .hasFieldOrPropertyWithValue("slug", article.getSlug())
                .hasFieldOrPropertyWithValue("published", article.getPublished());
    }
}
