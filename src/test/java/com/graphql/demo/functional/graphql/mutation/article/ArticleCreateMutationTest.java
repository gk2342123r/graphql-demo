package com.graphql.demo.functional.graphql.mutation.article;

import com.graphql.demo.entity.ArticleEntity;
import com.graphql.demo.entity.ArticleGroupEntity;
import com.graphql.demo.functional.graphql.base.BaseArticleTest;
import com.graphql.demo.generated.DgsConstants.MUTATION;
import com.graphql.demo.generated.client.ArticleCreateGraphQLQuery;
import com.graphql.demo.generated.client.ArticleCreateProjectionRoot;
import com.graphql.demo.generated.types.ArticleInput;
import com.graphql.demo.generated.types.ArticleType;
import com.netflix.graphql.dgs.client.codegen.GraphQLQueryRequest;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;

public class ArticleCreateMutationTest extends BaseArticleTest {
    @Test
    public void articleCreateSuccess() {
        String title = "some title";
        String text = "some text";
        String slug = "some-title";
        boolean published = true;
        ArticleGroupEntity group = articleGroupFactory.create();

        assertSame(null, articleRepository.findBySlug(slug));

        ArticleInput input =
                ArticleInput.newBuilder().title(title).slug(slug).text(text).published(published).articleGroupId(group.getId()).build();

        ArticleType articleType =
                dgsQueryExecutor.executeAndExtractJsonPathAsObject(
                        new GraphQLQueryRequest(
                                ArticleCreateGraphQLQuery.newRequest().input(input).build(),
                                new ArticleCreateProjectionRoot()
                                        .id()
                                        .title()
                                        .slug()
                                        .text()
                                        .published()
                                        .createdAt()
                                        .updatedAt()
                                        .articleGroup().id().name().published().createdAt().updatedAt())
                                .serialize(),
                        "data." + MUTATION.ArticleCreate,
                        ArticleType.class);

        assertNotNull(articleType);

        assertThat(articleType)
                .hasFieldOrPropertyWithValue("title", title)
                .hasFieldOrPropertyWithValue("slug", slug)
                .hasFieldOrPropertyWithValue("published", published)
                .hasFieldOrPropertyWithValue("text", text);

        assertThat(articleType.getArticleGroup())
                .hasFieldOrPropertyWithValue("id", group.getId())
                .hasFieldOrPropertyWithValue("name", group.getName());

        ArticleEntity article = articleRepository.findBySlug(slug);

        assertThat(article)
                .hasFieldOrPropertyWithValue("title", title)
                .hasFieldOrPropertyWithValue("slug", slug);
    }
}
