package com.graphql.demo.factory.article;

import com.graphql.demo.entity.ArticleEntity;
import net.datafaker.Faker;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Component
public class ArticleFactory {
    protected final Faker faker = new Faker();
    @PersistenceContext
    protected EntityManager em;

    @Transactional
    public ArticleEntity create() {
        ArticleEntity article = new ArticleEntity();

        article.setTitle(faker.name().name());
        article.setSlug(faker.internet().slug());
        article.setText(faker.lorem().characters(512));
        article.setPublished(faker.bool().bool());

        em.persist(article);
        em.flush();
        em.clear();

        return article;
    }
}
