package com.graphql.demo.factory.article;

import com.graphql.demo.entity.ArticleGroupEntity;
import net.datafaker.Faker;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Component
public class ArticleGroupFactory {
    protected final Faker faker = new Faker();
    @PersistenceContext
    protected EntityManager em;

    @Transactional
    public ArticleGroupEntity create() {
        ArticleGroupEntity group = new ArticleGroupEntity();

        group.setName(faker.name().name());
        group.setPublished(faker.bool().bool());

        em.persist(group);
        em.flush();
        em.clear();

        return group;
    }
}
